# Linko MX - Hadoop Master

Docker image for Hadoop master.

## Requirements

  - Docker 20

## Contributing

### Git config

```bash
$ git config --local "user.name" "myusernameatgitlab"
```

```bash
$ git config --local "user.email" "myemail@linko.mx"
```

## Building

### Local installation

For local installation use:

```bash
$ docker run \
    -u $(id -u):$(grep -w docker /etc/group | awk -F\: '{print $3}') \
    --rm \
    -w $(pwd) \
    -v /etc/group:/etc/group:ro \
    -v /etc/passwd:/etc/passwd:ro \
    -v $(pwd):$(pwd) \
    -v ${HOME}/.m2:${HOME}/.m2 \
    -v /var/run/docker.sock:/var/run/docker.sock \
    azul/zulu-openjdk-alpine:8u282 \
    ./mvnw -Djansi.force=true -ntp -U clean package
```

Then launch the Hadoop cluster with:

```bash
$ docker network create hadoop
```

```bash
$ for i in {1..3}; \
    do \
      docker run -d \
        --network=hadoop \
        --name=hadoop-slave-$i \
        --hostname=hadoop-slave-$i \
        registry.gitlab.com/linkomx/linkomx-hadoop:2.10.1; \
  done
```

```bash
$ docker run -d \
    -p 50070:50070 \
    -p 8088:8088 \
    --network=hadoop \
    --name=hadoop-master \
    --hostname=hadoop-master \
    linkomx-hadoop-master:2.10.1
```

