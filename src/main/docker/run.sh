#!/bin/bash

service ssh start

$HADOOP_HOME/bin/hdfs namenode -format

$HADOOP_HOME/sbin/start-dfs.sh

$HADOOP_HOME/sbin/start-yarn.sh

tail --retry -f $HBASE_HOME/logs/hadoop-*.log
